﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LaptopHandlerServer
{
    class Program
    {
        const int PORT_NO = 5000;

        public static MessagesHandler messagesHandler;
        public static StartupController startupController;
        public static SoundController SoundController;
        public static CommandsController CommandsController;

        internal static TcpClient connectedClient;
        private static NetworkStream clientNetworkStream;

        internal static byte[] receivingBuffer;

        static void Main(string[] args)
        {
            InitializeControllers();

            ApplicationEvents.OnClientDisconnected += ClientDisconnected;
            ApplicationEvents.OnSoundMuteChanged += SendIsMuted;

            IPAddress localAddress = IPAddress.Parse(GetLocalIPAddress());
            TcpListener server = new TcpListener(localAddress, PORT_NO);
            // we set our IP address as server's address, and we also set the port: 9999

            Console.WriteLine("Listening on " + localAddress.ToString() + "...");

            server.Start();  // this will start the server

            while (true)   //we wait for a connection
            {
                connectedClient = server.AcceptTcpClient();  //if a connection exists, the server will accept it
                Console.WriteLine("Client connected");
                clientNetworkStream = connectedClient.GetStream(); //networkstream is used to send/receive messages
                receivingBuffer = new byte[connectedClient.ReceiveBufferSize];

                SendCurrentVolume();
                Thread.Sleep(100);
                SendIsMuted();

                while (connectedClient.Connected)  //while the client is connected, we look for incoming messages
                {
                    int bytesRead = clientNetworkStream.Read(receivingBuffer, 0, receivingBuffer.Length);
                    //        //---convert the data received into a string---
                    string dataReceived = Encoding.ASCII.GetString(receivingBuffer, 0, bytesRead);

                    if (string.IsNullOrEmpty(dataReceived))
                    {
                        ClientDisconnected();
                    }
                    else
                    {
                        if (messagesHandler.HandleIncomingMessage(dataReceived))
                        {
                            Console.WriteLine("Received and handled: " + dataReceived);
                        }
                    }
                }

            }
        }

        private static void InitializeControllers()
        {
            Console.WriteLine("Initializing controllers...");
            messagesHandler = new MessagesHandler();
            startupController = new StartupController();
            startupController.Initialize();
            SoundController = new SoundController();
            CommandsController = new CommandsController();
        }

        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public static void SendMessageToClient(string messsageText)
        {
            if (connectedClient == null)
                return;

            if (!connectedClient.Connected)
                return;

            if (clientNetworkStream == null)
                return;

            // Convert string message to byte array.                 
            byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(messsageText);
            // Write byte array to socketConnection stream.                 
            clientNetworkStream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
        }

        public static void ClientDisconnected()
        {
            connectedClient.Close();
            Console.WriteLine("Client disconnected");
        }

        public static void SendCurrentVolume()
        {
            string currentVolumeMessage = ((int)EMessageType.CurrentSoundVolume).ToString() + "|" + SoundController.GetCurrentVolume() + Environment.NewLine;
            SendMessageToClient(currentVolumeMessage);
        }

        public static void SendIsMuted()
        {
            string currentSoundMuted = ((int)EMessageType.IsSoundMuted).ToString() + "|" + SoundController.GetSoundMuted() + Environment.NewLine;
            SendMessageToClient(currentSoundMuted);
        }
    }
}
