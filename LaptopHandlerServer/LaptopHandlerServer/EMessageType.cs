﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaptopHandlerServer
{
    public enum EMessageType
    {
        MuteSound,
        ChangeSoundVolume,
        CurrentSoundVolume,
        ClientDisconnected,
        Shutdown,
        AbortShutdown,
        PlayPreviousSong,
        PlayNextSong,
        PlayPause,
        IsSoundMuted,
    }
}
