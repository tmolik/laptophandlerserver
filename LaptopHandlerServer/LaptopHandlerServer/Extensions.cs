﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaptopHandlerServer
{
    public static class Extensions
    {

        public static bool IsWorkDay(this DateTime dateTime)
        {
            if (dateTime == null)
                return false;

            if (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday)
                return false;
            else
                return true;

        }
    }
}
