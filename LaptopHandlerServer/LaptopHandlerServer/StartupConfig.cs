﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaptopHandlerServer
{
    class StartupConfig
    {
        public List<StartupProgram> ProgramsToRunOnWeekDay = new List<StartupProgram>();
        public List<StartupProgram> ProgramsToRunOnWeekendDay = new List<StartupProgram>();
    }

    class StartupProgram
    {
        public string ProgramPath;
        public string ProgramArguments;

        public StartupProgram()
        {
        }
    }
}
