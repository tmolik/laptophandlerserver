﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaptopHandlerServer
{
    public static class ApplicationEvents
    {
        public static Action OnClientDisconnected = delegate { };
        public static Action OnSoundMuteChanged = delegate { };
    }
}
