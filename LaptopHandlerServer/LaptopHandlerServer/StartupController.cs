﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Web.Script.Serialization;

namespace LaptopHandlerServer
{
    class StartupController
    {
        public const string StartupConfigFilePath = "C:\\LaptopHandlerServer\\StartupConfig.json";

        public void Initialize()
        {

            string filePath = Path.Combine(Environment.CurrentDirectory, StartupConfigFilePath);

            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(filePath))
                {
                    // Read the stream to a string, and write the string to the console.
                    String line = sr.ReadToEnd();
                    StartupConfig startupConfig = new JavaScriptSerializer().Deserialize<StartupConfig>(line);
                    if (startupConfig != null)
                    {
                        OpenPrograms(startupConfig);
                    }
                    else
                    {
                        Console.WriteLine("Nie udalo sie zdeserializować configu");
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }

        public void OpenPrograms(StartupConfig startupConfig)
        {
            DateTime startDatetime = DateTime.Now;
            if (startDatetime.IsWorkDay())
            {
                if (startDatetime.Hour < 15)
                {
                    foreach (StartupProgram startupProgram in startupConfig.ProgramsToRunOnWeekDay)
                    {
                        OpenApplication(startupProgram.ProgramPath, startupProgram.ProgramArguments);
                    }

                }
            }
            else
            {
                foreach (StartupProgram startupProgram in startupConfig.ProgramsToRunOnWeekendDay)
                {
                    OpenApplication(startupProgram.ProgramPath, startupProgram.ProgramArguments);
                }
            }
        }

        public void OpenApplication(string applicationPath, string arguments = "")
        {
            try
            {
                if (arguments != "")
                    Process.Start(applicationPath, arguments);
                else
                    Process.Start(applicationPath);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldn't open " + applicationPath + ": " + e.Message);
            }
        }
    }
}
