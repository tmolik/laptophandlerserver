﻿using AudioSwitcher.AudioApi;
using AudioSwitcher.AudioApi.CoreAudio;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LaptopHandlerServer
{
    class SoundController : IObserver<AudioSwitcher.AudioApi.DeviceMuteChangedArgs>
    {
        private const int APPCOMMAND_VOLUME_MUTE = 173;
        private const int APPCOMMAND_VOLUME_UP = 175;
        private const int APPCOMMAND_VOLUME_DOWN = 174;

        private const int VK_MEDIA_NEXT_TRACK = 0xB0;// code to jump to next track
        private const int VK_MEDIA_PLAY_PAUSE = 0xB3;// code to play or pause a song
        private const int VK_MEDIA_PREV_TRACK = 0xB1;// code to jump to prev track

        private CoreAudioDevice playbackDevice;

        public SoundController()
        {
            playbackDevice = new CoreAudioController().DefaultPlaybackDevice;
            playbackDevice.MuteChanged.Subscribe(this);
        }

        public void SetVolume(int volume)
        {
            playbackDevice.Volume = volume;
        }


        public int GetCurrentVolume()
        {
            if (playbackDevice == null)
                return 0;

            return (int)playbackDevice.Volume;
        }

        public int GetSoundMuted()
        {
            if (playbackDevice == null)
                return 0;

            return playbackDevice.IsMuted ? 1 : 0;
        }

        public void MuteSound()
        {
            keybd_event(APPCOMMAND_VOLUME_MUTE, 0, 0, 0);
        }

        public void PlayPreviousSong()
        {
            keybd_event(VK_MEDIA_PREV_TRACK, 0, 1, 0);
        }

        public void PlayNextSong()
        {
            keybd_event(VK_MEDIA_NEXT_TRACK, 0, 1, 0);
        }

        public void PlayPauseSong()
        {
            keybd_event(VK_MEDIA_PLAY_PAUSE, 0, 1, 0);
        }


        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);

        public void OnNext(DeviceMuteChangedArgs value)
        {
            ApplicationEvents.OnSoundMuteChanged.Invoke();
        }

        public void OnError(Exception error)
        {
        }

        public void OnCompleted()
        {
        }
    }
}
