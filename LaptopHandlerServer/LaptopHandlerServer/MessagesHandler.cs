﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaptopHandlerServer
{
    class MessagesHandler
    {

        /// <summary>
        /// Messages will come in that way
        /// (int)EMessageType|value1|value2
        /// </summary>
        /// <param name="message"></param>
        public bool HandleIncomingMessage(string message)
        {
            if (string.IsNullOrEmpty(message))
                return false;

            string[] messageArguments = message.Split('|');

            int messageTypeInt;
            if (int.TryParse(messageArguments[0], out messageTypeInt))
            {
                EMessageType messageType = (EMessageType)messageTypeInt;

                switch (messageType)
                {
                    case EMessageType.MuteSound:
                        Program.SoundController.MuteSound();
                        return true;
                    case EMessageType.ChangeSoundVolume:
                        int volume;
                        if (int.TryParse(messageArguments[1], out volume))
                        {
                            Program.SoundController.SetVolume(volume);
                        }
                        return true;
                    case EMessageType.ClientDisconnected:
                        ApplicationEvents.OnClientDisconnected.Invoke();
                        return true;
                    case EMessageType.Shutdown:
                        int seconds;
                        if (int.TryParse(messageArguments[1], out seconds))
                        {
                            Program.CommandsController.Shutdown(seconds);
                        }
                        return true;
                    case EMessageType.AbortShutdown:
                        Program.CommandsController.AbortShutdown();
                        return true;
                    case EMessageType.PlayPreviousSong:
                        Program.SoundController.PlayPreviousSong();
                        return true;
                    case EMessageType.PlayNextSong:
                        Program.SoundController.PlayNextSong();
                        return true;
                    case EMessageType.PlayPause:
                        Program.SoundController.PlayPauseSong();
                        return true;
                    default:
                        break;
                }
            }

            return false;
        }


    }
}
